//Chat
function Submit(e){
	var key = e.keyCode;
	if(key == 13){
	/* code untuk enter */
	var text = document.getElementById("text").value;
	if(text.length == 0 || text == ""){
		alert("Isi text kosong!");
	}
	else{
	    document.getElementById("tempat").innerHTML += "You:<br>"+ text + "<br>";
	}
	document.getElementById("text").value = "";
	}
}

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = null;
	erase = true;
	
  } else if( x === 'log') {
	  print.value = Math.log10(evil(print.value));
	  erase = true;
  } else if( x === 'sin') {
	  print.value = Math.sin(evil(print.value));
	  erase = true;
  } else if( x === 'tan') {
	  print.value = Math.sin(evil(print.value));
	  erase = true;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

//Select	
	theme =
[
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

//set themes to localStorage
localStorage.setItem("theme",JSON.stringify(theme));
	
	//mySelect initiation
	myselect = $('.my-select').select2();
	
	//populate data in mySelect
	myselect.select2({
		'data' : JSON.parse(localStorage.getItem("theme"))
	});
	
	var arraysOfTheme = JSON.parse(localStorage.getItem("theme"));
	var currentTheme = arraysOfTheme[0]; //currentTheme
	console.log(currentTheme);
	var chosenTheme = currentTheme;
	
	if(localStorage.getItem("chosenTheme") != null){
	    currentTheme = JSON.parse(localStorage.getItem("chosenTheme"));
	}

	$('body').css({
	    "background-color":currentTheme.bcgColor,
		"font-color":currentTheme.fontColor
	});
	
	
	$('#apply').on('click', function(){ 
    var value = myselect.val();
    
    if(value < arraysOfTheme.length){
	    chosenTheme = arraysOfTheme[value];
		currentTheme = chosenTheme;
	}
    
	$('body').css({
	    "background-color": currentTheme.bcgColor,
		"font-color":currentTheme.fontColor
	});
	
	localStorage.getItem("currentTheme", JSON.stringify(currentTheme));
});


// END
